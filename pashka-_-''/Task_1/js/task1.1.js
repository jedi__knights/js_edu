'use strict'
/*--------(task_1_1)---------------------*/
/*1.1.1 Даны два числа a и b, вычичлите их сумму, разность, произведение, частное, сумму
квадратов, остаток от деления*/
function task_1_1_1() {
  //объявление переменных данных по условию
  var a = 3;
  var b = 4;
  console.log('====task 1.1.1================================================');
  console.log('a+b = ' + (a + b));
  console.log('a-b = ' + (a - b));
  console.log('a*b = ' + (a * b));
  console.log('a/b = ' + (a / b));
  console.log('a^2+b^2 = ' + (a*a + b*b));
  console.log('a%b = ' + (a%b));

}
function task_1_1_2(){
	console.log('====task 1.1.2================================================');
	var a = 3;
	var b = 4;
	var c;
	c=Math.sin(a)+Math.cos(2*b)*Math.abs(Math.tan(a))-Math.sqrt(a)+b*b;
	console.log(c);
}
function task_1_1_3(){
	console.log('====task 1.1.3================================================');
	var l=2,w=3,h=5;
	var s;
	s=2*(l*h)+2*(w*h);
	console.log('number '+(Math.round(s/16)));
}
function task_1_1_4(){
	console.log('====task 1.1.4================================================');
	var x=6, y=15, z=4;
	x+=(y-x++*z);
	console.log(x);
	z=(--x)-y*5;
	console.log(z);
	y/=(x+5%z);
	console.log(y);
	z=(x++)+y*5;
	console.log(z);
	x=y-(x++)*z;
	console.log(x);
}
function task_1_1_5(){
	console.log('====task 1.1.5================================================');
	var a=6, b=15, c=4;
	console.log((a+b+c)/3);
}
function task_1_1_6(){
	console.log('====task 1.1.6================================================');
	var r=3,h=6;
	console.log('V = ' +(Math.PI*r*r*h));
	console.log('S = ' +(2*Math.PI*r*h));
}
function task_1_1_7(){
	console.log('====task 1.1.7================================================');
	var x1=1,y1=1;
	var x2=4,y2=5;
	console.log('L = '+(Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))));
}
function task_1_1_8(){
	console.log('====task 1.1.8================================================');
	var r=3;
	console.log('S = ' +(Math.PI*r*r));
}
function task_1_1_9(){
	console.log('====task 1.1.9================================================');
	var a=2,b=3,c=5;
	console.log('x = ' +((c-b)/a));
}
function task_1_1_10(){
	console.log('====task 1.1.10================================================');
	var a=2,b=3,c=4;
	console.log('V = ' +(a*b*c));
}
/*--------(task_1_2)---------------------*/
function task_1_2_1(){
	var a=1,b=3;
	console.log('====task 1.2.1================================================');
	if(a>b)
		console.log('a>b');
	else
		if(a==b)
			console.log('a=b');
		else
			console.log('a<b');
}
function task_1_2_2(){
	var a=3,b=6,c=3;
	console.log('====task 1.2.2================================================');
	if(a>b)
		if(a>c)
			console.log('"a" is max');
		else
			if(a==c)
				console.log('"a" and "c" are max');
			else
				console.log('"c" is max');
	else
		if(a==b)
			if(a>c)
				console.log('"a" and "b" are max');
			else
				if(a==c)
					console.log('"a", "b" and "c" are max');
				else
					console.log('"c" is max');
		else
			if(b>c)
				console.log('"b" is max');
			else
				if(b==c)
					console.log('"b" and "c" are max');
				else
					console.log('"c" is max');				
	console.log(Math.max(a,b,c));		
}
function task_1_2_3(){
	var a=1,b=3,c=2,D;
	console.log('====task 1.2.3================================================');
	if(a==0)
		console.log('x = '+(-c/b));
	else
		if(c==0)
			console.log('x = 0 and x = '+(-b/a));
		else
			if(b==0)
				console.log('x = '+(Math.sqrt(-c/a))+' and x = '+(-Math.sqrt(-c/a)));
			else{
				D=b*b-4*a*c;
				if(D<0)
					console.log('error');
				else
					if(D==0)
						console.log('x = '+(-b/(2*a)));
					else
					{
						console.log('x1 = '+((-b-Math.sqrt(D))/(2*a)));
						console.log('x2 = '+((-b+Math.sqrt(D))/(2*a)));
					}
			}			
}
function task_1_2_4(){
	var a=2,b=1,c=5,max,min;
	max=Math.max(a,b,c);
	min=Math.min(a,b,c);	
	console.log('====task 1.2.4================================================');
	console.log('the difference '+(max-min));
}
function task_1_2_5(){
	var a=2,b=1,c=5,max,min;
	max=Math.max(a,b,c);
	min=Math.min(a,b,c);	
	console.log('====task 1.2.5================================================');
	console.log('the difference '+(max-min));
}
function task_1_2_6(){
	var N=10;	
	console.log('====task 1.2.6================================================');
	if(N&1)
		console.log('parts = '+(N));
	else
		console.log('parts = '+(N/2));
}
function task_1_2_7(){
	var a=385916;
	console.log('====task 1.2.7================================================');
	if((parseInt(a/100000%10)+parseInt(a/10000%10)+parseInt(a/1000%10))==(parseInt(a%10)+parseInt(a%100/10)+parseInt(a%1000/100)))
		console.log('happy ticket');
	else
		console.log('an unlucky ticket');	
}
/*--------(task_1_3)---------------------*/
function task_1_3_1(){
	var a=2,b=5,n;
	console.log('====task 1.3.1================================================');
	for(n=a;n<=b;n++)
		console.log(n);
	for(n=a;n<=b;n++)
		if(n&1)
			console.log(n);
}
function task_1_3_2(){
	var n=5,ch_s=1;
	do{
		ch_s=ch_s*n;
		n--;
	}while(n!=0);
	console.log('====task 1.3.2================================================');
	console.log('he number of combinations = '+(ch_s));
}
function task_1_3_3(){	
	var t='*',i;
	console.log('====task 1.3.3================================================');
	for(i=1;i<=4;i++){
		if(i==1)
			console.log(t+t+t+t+t+t+t+'\n'+t+t+t+t+t+t+t);
		if(i==2)
			console.log(t+'\n'+t+t+'\n'+t+' '+t+'\n'+t+t+t+t);
		if(i==3)
			console.log(' '+' '+t+' '+' '+'\n'+' '+t+t+t+' '+'\n'+t+t+t+t+t);
		if(i==4)
			console.log(' '+t+' '+'\n'+t+t+t+'\n'+' '+t+' ');
	}
}
function task_1_3_4(){
	var i,sum=0;
	console.log('====task 1.3.4================================================');
	for(i=1;i<=1000;i++)
		if(i%3==0 && i%5==0)
			sum=sum+i;
	console.log(sum);
}
function task_1_3_5(){
	var a1=1,a2=2,k=5,ak,d;
	d=a2-a1;
	ak=a1+d*(k-1);	
	console.log('====task 1.3.5================================================');
	console.log(ak);
}
function task_1_3_6(){
	var month=30, n=255,i=1;
	while(n>month){
		n=n-month;
		i++;
	}
	console.log('====task 1.3.6================================================');
	console.log((n)+('.')+(i));
}
function task_1_3_7(){	
	var d=13,m=9,y,i;
	console.log('====task 1.3.7================================================');
	for(i=1;i<=9999;i++)
		if(i%4==0)
			console.log((d-1)+'/'+m+'/'+i);
		else
			console.log((d)+'/'+m+'/'+i);
}
function task_1_3_8(){	
	var i=0,n,r=1,a=6738921;	
	do{
		n=parseInt((a%(10*r))/(1*r));
		if(n==6 || n==9 || n==0)
			i=i+1;
		if(n==8)
			i=i+2;
		r*=10;		
	}while(a%r!=a);
	console.log('====task 1.3.8================================================');
	console.log(i);
}
/*--------(task_1_4)---------------------*/
function task_1_4_1(){
	var i=0,q,r=1,a,n=15;
	a=n.toString(2)
	do{
		q=parseInt((a%(10*r))/(1*r));
		if(q==1)
			i++;
		r*=10;		
	}while(a%r!=a);
	console.log('====task 1.4.1================================================');
	console.log(i);
}
function task_1_4_2(){
	var i,n,r=1,sum=0;
	for(i=0;i<=100;i++){
		do{
			n=parseInt((i%(10*r))/(1*r));
			sum=sum+n;
			r*=10;		
		}while(i%r!=i);
	}
	console.log('====task 1.4.2================================================');
	console.log(sum);
}