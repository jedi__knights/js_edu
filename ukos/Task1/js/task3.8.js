'use strict'
 function task_1_3_8() {
 	console.log("Task 1.3.8");
 	console.log('');
 	/*  1.3.8 Однажды в просторах рунета появился следующий ребус:
     157892 = 3
     203516 = 2
     409578 = 4
     236271 = ?
	Никто так и не смог его разгадать. Позже оказалось, что число в правом столбце равно сумме
	"кругляшей", которые есть в цифрах числа, расположенного слева. Ваша задача написать
	программу, которая определяет, сколько кругляшей в числе.

		1 2 3 4 5 6 7 8 9 0
	*/
	var x=236271, s=0;
	var a, k;

	while(x!=0) {
		a=x%10;
		x/=10;
		x=Math.floor(x);
		switch(a) {

			case 8: k=2;
			break;

			case 6:
			case 9:
			case 0: k=1;
			break;
			
			default: k=0;
			break;
		}
		s=s+k;
	}
	console.log('сумма кругляшей - '+(s));
	


}