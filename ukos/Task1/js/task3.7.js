'use strict'
 function task_1_3_7() {
 	console.log("Task 1.3.7");
 	console.log('');
 	/*  1.3.7 В григорианском календаре високосным является:
		год, номер которого делится нацело на 400
		год, номер которого делится на 4, но не делится на 100
		входные данные целое число от 1 до 9999 включительно, которое обозначает номер года нашей эры.
		Нужно вывести дату Дня программиста в формате DD/MM/YYYY, где DD — число, MM — номер
		месяца (01 — январь, 02 — февраль, ..., 12 — декабрь), YYYY — год в десятичной записи.
	*/
	var a=[255], n=255, year=1601;


	for(var i=1; i<=4;) {
		for(var k=0; k<255;) {
			for(var j=1; j<=31; j++) {
				if(i==1) { a[k]=j+' января '; k++; }
				if(i==3) { a[k]=j+' марта '; k++; }
				if(i==5) { a[k]=j+' мая '; k++; }
				if(i==7) { a[k]=j+' июля '; k++; }
				if(i==8) { a[k]=j+' августа '; k++; }
				if(i==10) { a[k]=j+' октября '; k++; }
				if(i==12) { a[k]=j+' декабря '; k++; }
				
			}
			for(j=1; j<=30; j++) {
				if(i==2) { 
					if(year%400==0) {
						if(j==29) j=31;
					} else if(j==28) j=31;
					a[k]=j+' февраля'; k++;
				 }
				if(i==4) { a[k]=j+' апреля '; k++; }
				if(i==6) { a[k]=j+' июня '; k++; }
				if(i==9) { a[k]=j+' сентября '; k++; }	
				if(i==11) { a[k]=j+' ноября '; k++; }			
			}
			i++;
		}
	}
	console.log((n)+" день -  "+a[n]+''+(year)+' г.');

}

				