'use strict'
 function task_1_1_6() {
 	console.log("Task 1.1.6");
 	console.log('');

 	/*  1.1.6 Напишите программу расчета объема - V и площади поверхности - S цилиндра.*/

	var R=5, H=10;
	var V = Math.PI * Math.pow(R,2)*H;
	var S = 2 * Math.PI * R * H;
	console.log("Объем цилиндра = " + (V));
	console.log("Площадь поверхности цилиндра = " + (S));

 }