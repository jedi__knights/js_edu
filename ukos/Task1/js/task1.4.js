'use strict'
 function task_1_1_4() {
 	console.log("Task 1.1.4");
 	console.log('');
 	/*  1.1.4 Создайте 3 переменные   x = 6, y = 15, и z = 4:
		Выполните и отобразите результат следующих операций для этих переменных:
		· x += y - x++ * z ;
		· z = -- x - y * 5 ;
		· y /= x + 5 % z ;
		· z = x++ + y * 5 ;
		· x = y - x++ * z ;.*/
	var x = 6, y = 15, z = 4;

	console.log("x =" + (x += y - x++ * z));
	console.log("z =" + (z = -- x - y * 5));
	console.log("y =" + (y /= x + 5 % z));
	console.log("z =" + (z = x++ + y * 5));
	console.log("x =" + (x = y - x++ * z));

 }