//Task 1
function task_1_1_1(){
  var a = 3, b = 4;
  console.log('====task 1.1.1================================================');
  console.log('a+b = ' + (a + b));
  console.log('a-b = ' + (a - b));
  console.log('a*b = ' + (a * b));
  console.log('a/b = ' + (a / b));
  console.log('a^2+b^2 = ' + (a*a + b*b));
  console.log('a%b = ' + (a%b));
}
function task_1_1_2(){
    var a=1, b=3;
    console.log('====task 1.1.2================================================');
    console.log('sin(a)+ cos(2b)*|tan(a)|-(a)^(1/2) + b^2'+ '=' +(Math.sin(a) + Math.cos(2*b)*Math.abs(Math.tan(a)) - Math.pow(a,1/2) + Math.pow(b,2)));
}
function task_1_1_3(){
    var l=10, h=3;
    console.log('====task 1.1.3================================================');
    console.log('number:' + (l*h/16)); 
}
function task_1_1_4(){
    var x = 6, y = 15, z = 4; 
    console.log('====task 1.1.4================================================');
    console.log(x += y - x++ * z);
    console.log(z = -- x - y * 5);
    console.log(y /= x + 5 % z);
    console.log(z = x++ + y * 5);
    console.log(x = y - x++ * z);
}
function task_1_1_5(){
    var a = 2, b = 4, c = 100;
    console.log('====task 1.1.5================================================');
    console.log('(2+4+100)/3=' + ((a + b + c)/3));
}
function task_1_1_6(){
    var r = 3, h = 9;
    console.log('====task 1.1.6================================================');
    console.log('V=' + (3.14 * r * r * h));
    console.log('S=' + (3.14 * r * 2 * h + 2 * 3.14 * r * r));
}
function task_1_1_7(){
    var x1 = 1, y1 = 1, x2 = 5, y2 = 10;
    console.log('====task 1.1.7================================================');
    console.log('L=' + (Math.pow(x2 * x2 + y2 * y2,1/2) - Math.pow(x1 * x1 + y1 * y1,1/2)));
}
function task_1_1_8(){
    var r = 2;
    console.log('====task 1.1.8================================================');
    console.log('S=' + (3.14 * r * r));
}
function task_1_1_9(){
    var a = 2, b = 5, c = 3;
    console.log('====task 1.1.9================================================');
    console.log('x=' + ((c - b)/a));
}
function task_1_1_10(){
    var a = 2, b = 4, c = 6;
    console.log('====task 1.1.10================================================');
    console.log('V=' + (a * b * c));
}
//Task 2
function task_1_2_1(){
    var a = 10, b = 1;
    console.log('====task 1.2.1================================================');
    console.log('a=' + (a) + ' b=' + (b));
    if(a > b){
        console.log('a > b');
    }
    else{
        console.log('a < b');
    }
}
function task_1_2_2(){
    var a =40 , b = 150, c = 521;
    console.log('====task 1.2.2================================================');
    console.log('a=' + (a) + '\n' + 'b=' + (b) + '\n' + 'c=' + (c));
    if(a > b && b > c )
        console.log('1 bunch');
    if(a > c && c > b)
        console.log('1 bunch');
    if(b > a && a > c )
        console.log('2 bunch');
    if(b > c && c > a)
        console.log('2 bunch');
    if(c > b && b > a )
        console.log('3 bunch');
    if(c > a && a > b)
        console.log('3 bunch');
}
function task_1_2_3(){
    var a = 3, b = 4, c = 1, d;
    console.log('====task 1.2.3================================================');
    d=b*b - 4*a*c;
    if(d >= 0){
        console.log('x1=' + ((-b - Math.pow(d,1/2))/(2*a)));
        console.log('x2=' + ((-b + Math.pow(d,1/2))/(2*a)));
    }
    else
        console.log('error');
}
function task_1_2_4(){
    var a = 1000, b = 250, c = 500;
    console.log('====task 1.2.4================================================');
    console.log('a=' + (a) + '\n' + 'b=' + (b) + '\n' + 'c=' + (c));
    if(a > b && b > c )
        console.log((a - c));
    if(a > c && c > b)
        console.log((a - b));
    if(b > a && a > c )
        console.log((b - c));
    if(b > c && c > a)
        console.log((b - a));
    if(c > b && b > a )
        console.log((c - a));
    if(c > a && a > b)
        console.log((c - b));
}
function task_1_2_6(){
    var m = 11;
    console.log('====task 1.2.6================================================');
    if(m%2 == 0)
        console.log((m/2));
    else
        console.log(m);
}
function task_1_2_7(){
    var n = 784521, x1, x2, x3, x4, x5, x6;
    console.log('====task 1.2.7================================================');
    x1 = Math.floor(n / 100000);
    x2 = Math.floor(n / 10000) % 10;
    x3 = Math.floor(n / 1000) % 10;
    x4 = Math.floor(n / 100) % 10;
    x5 = Math.floor(n / 10) % 10;
    x6 = n % 10;
    console.log(x1 + x2 + x3);
    console.log(x5 + x4 + x6);
        if (x1 + x2 + x3 == x4 + x5 + x6)
            console.log('Happy');
        else
            console.log('Unhappy');
    
}
function task_1_3_1(){
    console.log('====task 1.3.1================================================');
    var a = 1, b = 12, sum = 0;
        for(a; a <= b; a++)
            sum += a;
    console.log(sum);
    for(a = 1; a <= b; a++){
        if(a % 2 != 0)    
            console.log(a);
    }
}
function task_1_3_2(){
    console.log('====task 1.3.2================================================');
    var x = 0, sum = 0;
        do{
            sum += x;
            x++;
        }
        while(x<=10)
    console.log(sum);        
}
function task_1_3_3(){
    var x = '*';
    console.log('====task 1.3.3================================================');
    for(var i = 0; i <= 4; i++){
        if(i == 0)
            console.log(x+x+x+x+x+x+'\n'+x+' '+' '+' '+' '+x+'\n'+x+x+x+x+x+x);
        if(i == 2)
            console.log(x+'\n'+x+x+'\n'+x+' '+x+'\n'+x+' '+' '+x+'\n'+x+x+x+x+x);
        if(i == 3)
            console.log(' '+' '+x+' '+' '+'\n'+' '+x+' '+x+' '+'\n'+x+x+x+x+x);
        if(i == 4)
            console.log(' '+' '+x+' '+' '+'\n'+' '+x+' '+x+' '+'\n'+x+' '+' '+' '+x+'\n'+' '+x+' '+x+' '+' '+'\n'+' '+' '+x+' '+' ');
    }
}
function task_1_3_4(){
    console.log('====task 1.3.4================================================');
    var a = 1, b = 1000, sum = 0;
        for(a;a <= b; a++){
                if(a % 3 == 0 && a % 5 == 0)
                    sum +=a;
            }
    console.log(sum);
}
function task_1_3_5(){
    console.log('====task 1.3.5================================================');
    var a = 3, b = 9, n = 10, i = 1, k = 1;
    for(i;i <= 10; i++){
        k *= a;
        console.log(k);
    }
}
function task_1_3_8(){
    var n = 40498412874611, i, k, m = 0, k1, l = -1;
    console.log('====task 1.3.8================================================');
        for(i = 1; i > 0; i *= 10){
            k = Math.floor(n % (i*10)/i);
            k1 = Math.floor(n/i);
            l++;
            if(k1 == 0) break;
        }
    console.log(l);
        for(i = 1; i <=Math.pow(10,l-1); i *= 10){
            k = Math.floor(n % (i*10)/i);
                if(k == 0 || k == 6 || k == 9)
                    m++;
                if(k == 8)
                    m +=2;
        }
    console.log(m);
}
function task_1_4_1(){
    console.log('====task 1.4.1================================================');
    var n = 17, i = 0, h, k = 0;    
        while(n > 0){
            h += n % 2;
            n = Math.floor(n / 2);    
        }
    console.log(h);
}
function task_1_4_2(){
    console.log('====task 1.4.2================================================');
    var n, i, k, m = 0, k1, l = -1, sum = 0;
        for(n = 0; n <= 145; n++){
            for(i = 1; i > 0; i *= 10){
                k1 = Math.floor(n/i);
                if(k1 == 0) break;
                k = Math.floor(n % (i*10)/i);
                sum += k;
                console.log(k);
            }
            
        }
    console.log(sum);
}
